// @flow
import Raven from 'raven-js'

// types
import type { RavenOptions } from 'raven-js'
type GuardXOptions = RavenOptions & {
  apiKey: string,
  debugApiKey?: string,
}

Raven.setup = (opts: GuardXOptions) => {
  const options = { ...opts }
  if (!options.apiKey || options.apiKey.length <= 0) {
    console.error('You have to set app apiKey')
  }
  if (!options.version || options.version.length <= 0) {
    console.error('You have to set app version')
  }

  const { appCodeName, appName, appVersion, cookieEnabled, language, onLine,
          platform, product, productSub, userAgent, vendor, vendorSub } = navigator

  options.tags = {
    ...options.tags,
    version: options.version,
    appCodeName,
    appName,
    appVersion,
    cookieEnabled,
    language,
    onLine,
    platform,
    product,
    productSub,
    userAgent,
    vendor,
    vendorSub,
    origin: location.origin,
  }

  if (options.debug) {
    Raven.setEnvironment('debug')
    options.apiKey = options.debugApiKey
  } else {
    Raven.setEnvironment('production')
  }

  if (options.version) {
    Raven.setRelease(options.version)
  }

  if (options.stackdriver) {
    Raven.setDataCallback((data) => {
      fetch(`https://guardx.devx.agency/api/${options.apiKey}/log`, {
        method: "POST",
        mode: 'no-cors',
        body: JSON.stringify({
          logName: data.extra.logName,
          resource: data.extra.resource,
          severity: data.extra.severity,
          entries: [
            {
              logName: data.extra.logName,
              resource: data.extra.resource,
              ...data
            }
          ]
        })
      })
      return data
    })
  }

  Raven.config(`https://guardx.devx.agency/${options.apiKey}`, options)

  Raven.install()
}

export default Raven
