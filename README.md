# GuardX Ligrary
Javascript functions to help with development.

## Install
```git
yarn add https://github.com/devx-agency/guardx-library
```

## Usage
### GuardXLogger
```javascript
import GuardXLogger from 'guardx-library'
```

function | specification
---|---
captureException | (ex: Error \| ErrorEvent \| string, options?: GuardXOptions) => GuardXLogger
captureMessage | (msg: string, options?: GuardXOptions) => GuardXLogger
captureBreadcrumb | (crumb: { message?: string, category?: string, level?: LogLevel, data?: any, type?: 'navigation' \| 'http' }) => GuardXLogger
setExtraContext | (context?: Object) => GuardXLogger
setUser | (user?: { [key: string]: string \| number \| boolean \| null \| void }) => GuardXLogger
setVersion | (string) => GuardXLogger
install | (config: {...}) => void

#### LogLevel
'critical' | 'error' | 'warning' | 'info' | 'debug' | 'warn' | 'log'

#### GuardXOptions:

    /** The log level associated with this event. Default: error */
    level?: LogLevel;

    /** The name of the logger used by GuardX. Default: javascript */
    logger?: string;

    /** The environment of the application you are monitoring with GuardX */
    environment?: string;

    /** The release version of the application you are monitoring with GuardX */
    release?: string;

    /** The name of the server or device that the client is running on */
    serverName?: string;

    /** List of messages to be filtered out before being sent to GuardX. */
    ignoreErrors?: (RegExp | string)[];

    /** Similar to ignoreErrors, but will ignore errors from whole urls patching a regex pattern. */
    ignoreUrls?: (RegExp | string)[];

    /** The inverse of ignoreUrls. Only report errors from whole urls matching a regex pattern. */
    whitelistUrls?: (RegExp | string)[];

    /** An array of regex patterns to indicate which urls are a part of your app. */
    includePaths?: (RegExp | string)[];

    /** Additional data to be tagged onto the error. */
    tags?: {
      [id: string]: string;
    };

    /** set to true to get the stack trace of your message */
    stacktrace?: boolean;

    extra?: any;

    /** In some cases you may see issues where GuardX groups multiple events together when they should be separate entities. In other cases, GuardX simply doesn’t group events together because they’re so sporadic that they never look the same. */
    fingerprint?: string[];

    /** An array of strings representing keys that should be scrubbed from the payload sent to GuardX */
    sanitizeKeys?: string[];

    /** A function which allows mutation of the data payload right before being sent to GuardX */
    dataCallback?: (data: any) => any;

    /** A callback function that allows you to apply your own filters to determine if the message should be sent to GuardX. */
    shouldSendCallback?: (data: any) => boolean;

    /** By default, GuardX does not truncate messages. If you need to truncate characters for whatever reason, you may set this to limit the length. */
    maxMessageLength?: number;

    /** By default, GuardX will truncate URLs as they appear in breadcrumbs and other meta interfaces to 250 characters in order to minimize bytes over the wire. This does *not* affect URLs in stack traces. */
    maxUrlLength?: number;

    /** By default, GuardX captures all unhandled promise rejections using standard `unhandledrejection` event. If you want to disable this behaviour, set this option to `false` */
    captureUnhandledRejections?: boolean;

    /** Override the default HTTP data transport handler. */
    transport?: (options: GuardXTransportOptions) => void;

    /** Append headers to the fetch or XMLHttpRequest request. Should be in a form of hash, were value can be string or function */
    headers?: {
      [key: string]: string | Function;
    };

    /** `fetch` init parameters */
    fetchParameters?: {
      [key: string]: string | Function;
    };

    /** Allow use of private/secretKey. */
    allowSecretKey?: boolean;

    /** Enables/disables instrumentation of globals. */
    instrument?: boolean | GuardXInstrumentationOptions;

    /** Enables/disables automatic collection of breadcrumbs. */
    autoBreadcrumbs?: boolean | AutoBreadcrumbOptions;

    /** By default, GuardX captures as many as 100 breadcrumb entries. If you find this too noisy, you can reduce this number by setting maxBreadcrumbs. Note that this number cannot be set higher than the default of 100. */
    maxBreadcrumbs?: number;

    /** A function that allows filtering or mutating breadcrumb payloads. Return false to throw away the breadcrumb. */
    breadcrumbCallback?: (data: any) => any;

    /**
     * A sampling rate to apply to events. A value of 0.0 will send no events, and a value of 1.0 will send all events (default).
     */
    sampleRate?: number;

    /**
     * By default, GuardX.js attempts to suppress duplicate captured errors and messages that occur back-to-back.
     * Such events are often triggered by rogue code (e.g. from a `setInterval` callback in a browser extension),
     * are not actionable, and eat up your event quota.
     */
    allowDuplicates?: boolean;

    /**
     * If set to true, GuardX.js outputs some light debugging information onto the console.
     */
    debug?: boolean;

## Thanks to T & M!
